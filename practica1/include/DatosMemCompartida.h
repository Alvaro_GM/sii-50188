#pragma once 
#include "../include/Esfera.h"
#include "../include/Raqueta.h"

class DatosMemCompartida
{       
public:         
      Esfera esfera;
      Raqueta raqueta1;
      Raqueta raqueta2;
      int accion; //1 arriba, 0 nada, -1 abajo
};
