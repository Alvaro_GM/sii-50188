#include "Disparo.h"
#include "glut.h"
#include <iostream>

Disparo::Disparo(){}

Disparo::Disparo(float posx,float posy,float velx,float vely,float r){

  posicion.x=posx;
  posicion.y=posy;
  velocidad.x=velx;
  velocidad.y=vely;
  radio=r;
  //std::cout<<"Disparo Creado"<<std::endl;
}
Disparo::~Disparo(){/*std::cout<<"Disparo Destruido"<<std::endl;*/}

void Disparo::Dibuja(){

	glColor3ub(47,79,79);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(posicion.x,posicion.y,-1);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}
void Disparo::Mueve(float t){

  posicion=posicion+velocidad*t;
}
