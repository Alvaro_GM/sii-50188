// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>
#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	
}

Esfera::~Esfera()
{
	
}



void Esfera::Dibuja()
{
	glColor3ub(218,165,32);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,150,150);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
centro=centro+velocidad*t;
if(radio>0.05f)
	radio=radio-0.005f;
else
	radio=0.5f;
}
