#include <stdio.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

/*El programa logger es el encargado de crear la tubería (mkfifo), abrirla en modo lectura (open) y destruirla (close y unlink)*/


#include <string.h>
#include "glut.h"
#include <fstream>
#include <stdlib.h>
#include <math.h>
using namespace std;

int main(/*int argc,char* argv[]*/){
	char cad[200];

	//creo la tuberia
	mkfifo("/tmp/TuberiaLogger",0777);
	
	//abro solo lectura
	int fd = open("/tmp/TuberiaLogger",O_RDONLY);

	/*El logger entra en un bucle infinito de recepción de datos (read) e imprime el mensaje por salida 		estándar*/
	while(1){
		char cad[200];
		read(fd, cad, sizeof(cad));
			if(cad[0]=='J'){
				printf("%s\n",cad);
				sprintf(cad," ");
			}
		}

	//cierro la tuberia
	close(fd);

	//destruyo la tuberia
	unlink("/tmp/TuberiaLogger");
	return 0;
}
