#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <stdio.h>


int main(){
  
  int crear_ok;
  int abierta_ok;
  int cerrada_ok;
  int datos[2];
  int dato_leido;
  
  mkfifo("/tmp/logger", 0666 );
  abierta_ok = open("/tmp/logger", O_RDONLY);
  if(abierta_ok < 0){
    printf("ERROR al abrir FIFO.\n"); 
    return 0;
  }
  while(1){
    dato_leido = read(abierta_ok, datos, sizeof(datos));
    if(dato_leido)
      printf("Jugador %d marca 1 punto, lleva un total de %d puntos\n", datos[0], datos[1]);
  }
  cerrada_ok = close(abierta_ok);
  unlink("/tmp/logger");
  return 0;
}
